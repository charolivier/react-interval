import React, { Component } from 'react';

class Clock extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date: new Date(),
        };
    }

    componentDidMount() {
        this.play = setInterval(() => {
            this.setState({
                date: new Date(),
            }, 1000);
        });
    }

    componentWillUnmount() {
        clearInterval(this.play);
    }

    render() {
        return (<div className="Clock">{ this.state.date.toLocaleTimeString() }</div>);
    }
}

export default Clock;
